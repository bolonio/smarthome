"use strict";

module.exports = function(sequelize, DataTypes) {
  var Room = sequelize.define("Room", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    //userid: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.STRING
  });
  return Room;
};