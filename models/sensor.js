"use strict";

module.exports = function(sequelize, DataTypes) {
  var Sensor = sequelize.define("Sensor", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    //roomid: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.STRING
  });
  return Sensor;
};