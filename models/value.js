"use strict";

module.exports = function(sequelize, DataTypes) {
  var Value = sequelize.define("Value", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    //sensorid: DataTypes.INTEGER,
    value: DataTypes.FLOAT
  });
  return Value;
};