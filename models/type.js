"use strict";

module.exports = function(sequelize, DataTypes) {
  var Type = sequelize.define("Type", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    type: DataTypes.STRING
  });
  return Type;
};