var express = require('express');
var router  = express.Router();
var models  = require('./models');

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

setInterval(function(){
  models.Sensor.findAll(
  { 
    where: { TypeId: [1, 2]}, 
    include: [{ all: true, nested: true }]
  })
  .then(function(sensors) {
    if (sensors) {
      sensors.forEach(function (sensor) {
        var currentVal = 0;
        sensor.Values.forEach(function (value) {
            currentVal = value.value;
        });
        var maxRandom = currentVal+5;
        var minRandom = currentVal-5;
        var newVal = getRandomInt(minRandom, maxRandom);
        models.Value.create({
          SensorId: sensor.id,
          value: newVal,
        }).then(function(value_response){
          if(value_response)
            console.log('Value inserted for sensor with name: ' + sensor.name);
          else
            console.log('ERROR: Value not inserted for sensor with name: ' + sensor.name);
        });
      });
    }
    else
      console.log('ERROR: Sensor not found');
  });
}, 1000*30); 