var express = require('express');
var router  = express.Router();

router.get('/', function(req,res){
  res.json({ 'error' : false, 'message' : 'Welcome to the Smart Home REST API' });
});

module.exports = router;