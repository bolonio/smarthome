var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all values
router.get('/', function(req, res) {
  models.Value.findAll().then(function(values) {
    if (values)
      res.json(values);
    else
      res.status(401).send('value not found');
  });
});

// Get one value information by valueid
router.get('/:valueid',function(req,res){
  models.Value.findOne(
    {
      where: {id: req.params.valueid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(value) {
    if (value)
      res.json(value);
    else
      res.status(401).send('Value not found');
  });
});

// Delete one value by valueid
router.delete('/:valueid',function(req,res){
  models.Value.findOne({where: {id: req.params.valueid}}).then(function(value) {
    if (value) {
      value.destroy();
      res.status(200).send('value deleted');
    }
    else
      res.status(401).send('value not found');
  });
});

// Update one value by valueid
router.put('/:valueid',function(req,res){
  models.Value.findOne({where: {id: req.params.valueid}}).then(function(value) {
    if (value) {
      value.update({
        value: req.body.value
      }).then(function(response) {
        if(response)
          res.status(200).send(response);
        else
          res.status(401).send('ERROR: Value not updated');
      });
    }
  });
});

// Insert one value
router.post('/',function(req,res){
  return models.Value.upsert({
    sensorid: req.body.sensorid,
    value: req.body.value,
  }).then(function(test){
    if(test)
      res.status(200).send('value inserted');
    else
      res.status(401).send('ERROR: Value not inserted');
  });
});

module.exports = router;
