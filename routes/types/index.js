var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all types
router.get('/', function(req, res) {
  models.Type.findAll().then(function(types) {
    if (types)
      res.json(types);
    else
      res.status(401).send('type not found');
  });
});

// Get one type information by typeid
router.get('/:typeid',function(req,res){
  models.Type.findOne(
    {
      where: {id: req.params.typeid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(type) {
    if (type)
      res.json(type);
    else
      res.status(401).send('Type not found');
  });
});

// Delete one type by typeid
router.delete('/:typeid',function(req,res){
  models.Type.findOne({where: {id: req.params.typeid}}).then(function(type) {
    if (type) {
      type.destroy();
      res.status(200).send('type deleted');
    }
    else
      res.status(401).send('type not found');
  });
});

// Update one type by typeid
router.put('/:typeid',function(req,res){
  models.Type.findOne({where: {id: req.params.typeid}}).then(function(type) {
    if (type) {
      type.sensorid = req.body.sensorid;
      type.type = req.body.type;
      type.save(function(err) {
        if (err)
            res.status(401).send('ERROR: type not updated');
        else
          res.status(200).send('type updated');
      });
    }
    else
      res.status(401).send('type not found');
  });
});

// Insert one type
router.post('/',function(req,res){
  return models.Type.upsert({
    sensorid: req.body.sensorid,
    type: req.body.type,
  }).then(function(test){
    if(test)
      res.status(200).send('type inserted');
    else
      res.status(401).send('ERROR: type not inserted');
  });
});

module.exports = router;
