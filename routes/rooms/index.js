var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all rooms
router.get('/', function(req, res) {
  models.Room.findAll({ include: [{ all: true, nested: true }] })
  .then(function(rooms) {
    if (rooms)
      res.json(rooms);
    else
      res.status(401).send('Rooms not found');
  });
});

// Get one room information by roomid
router.get('/:roomid',function(req,res){
  models.Room.findOne(
    {
      where: {id: req.params.roomid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(room) {
    if (room)
      res.json(room);
    else
      res.status(401).send('Room not found');
  });
});

// Get rooms by userid
router.get('/user/:userid',function(req,res){
  models.Room.findAll({where: {UserId: req.params.userid}}).then(function(rooms) {
    if (rooms)
      res.json(rooms);
    else
      res.status(401).send('room not found');
  });
});

// Delete one room by roomid
router.delete('/:roomid',function(req,res){
  models.Room.findOne({where: {id: req.params.roomid}}).then(function(room) {
    if (room) {
      room.destroy();
      res.status(200).send('room deleted');
    }
    else
      res.status(401).send('room not found');
  });
});

// Update one room by roomid
router.put('/:roomid',function(req,res){
  models.Room.findOne({where: {id: req.params.roomid}}).then(function(room) {
    if (room) {
      room.update({
        UserId: req.body.userid,
        name: req.body.name,
        description: req.body.description,
      }).then(function(response) {
        if(response)
          res.status(200).send(response);
        else
          res.status(401).send('ERROR: Room not updated');
      });
    }
  });
});


// Insert one room
router.post('/',function(req,res){
  return models.Room.create({
    UserId: req.body.userid,
    name: req.body.name,
    description: req.body.description,
  }).then(function(response){
    if(response)
      res.status(200).send(response);
    else
      res.status(401).send('ERROR: Room not inserted');
  });
});

module.exports = router;
