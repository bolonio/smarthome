var express = require('express');
var router  = express.Router();
var models  = require('../../models');

// Get all sensors
router.get('/', function(req, res) {
  models.Sensor.findAll({ include: [{ all: true, nested: true }] })
  .then(function(sensors) {
    if (sensors)
      res.json(sensors);
    else
      res.status(401).send('Sensors not found');
  });
});

// Get one sensor information by sensorid
router.get('/:sensorid',function(req,res){
  models.Sensor.findOne(
    {
      where: {id: req.params.sensorid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(sensor) {
    if (sensor)
      res.json(sensor);
    else
      res.status(401).send('Sensor not found');
  });
});

// Get sensors by roomid
router.get('/room/:roomid',function(req,res){
  models.Sensor.findAll({where: {RoomId: req.params.roomid}}).then(function(sensors) {
    if (sensors)
      res.json(sensors);
    else
      res.status(401).send('room not found');
  });
});

// Get sensors by userid
router.get('/user/:userid',function(req,res){
  models.Sensor.findAll({
    include: [{ model: models.Room }],
    where: {RoomId: req.params.roomid}}
  ).then(function(sensors) {
    if (sensors)
      res.json(sensors);
    else
      res.status(401).send('room not found');
  });
});

// Delete one sensor by sensorid
router.delete('/:sensorid',function(req,res){
  models.Sensor.findOne({where: {id: req.params.sensorid}}).then(function(sensor) {
    if (sensor) {
      sensor.destroy();
      res.status(200).send('Sensor deleted');
    }
    else
      res.status(401).send('Sensor not found');
  });
});

// Update one sensor by sensorid
router.put('/:sensorid',function(req,res){
  models.Sensor.findOne({where: {id: req.params.sensorid}}).then(function(sensor) {
    if (sensor) {
      sensor.update({
        RoomId: req.body.RoomId,
        name: req.body.name,
        description: req.body.description,
        TypeId: req.body.TypeId
      }).then(function(response) {
        if(response)
          res.status(200).send(response);
        else
          res.status(401).send('ERROR: Sensor not updated');
      });
    }
  });
});


// Insert one sensor
router.post('/',function(req,res){
  return models.Sensor.create({
    RoomId: req.body.RoomId,
    name: req.body.name,
    description: req.body.description,
    TypeId: req.body.TypeId
  }).then(function(response){
    if(response) {
      // Creating a new value for the new sensor
      var sensor_id = response.id;
      return models.Value.create({
        SensorId: sensor_id,
        value: 0,
      }).then(function(value_response){
        if(value_response)
          res.status(200).send(value_response);
        else
          res.status(401).send('ERROR: Value not inserted');
      });
      //res.status(200).send(response);
    }
    else
      res.status(401).send('ERROR: Sensor not inserted');
  });
});

module.exports = router;
