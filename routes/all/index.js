var express = require('express');
var router  = express.Router();
var models  = require('../../models');
var crypto = require('crypto');

// Get all users information
router.get('/', function(req, res) {
  models.User.findAll({ include: [{ all: true, nested: true }] })
  .then(function(users) {
    if (users)
      res.json(users);
    else
      res.status(401).send('Users not found');
  });
});

// Get one user information by userid
router.get('/:userid',function(req,res){
  models.User.findOne(
    {
      where: {id: req.params.userid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(user) {
    if (user)
      res.json(user);
    else
      res.status(401).send('User not found');
  });
});

module.exports = router;