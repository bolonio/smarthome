var express = require('express');
var router  = express.Router();
var models  = require('../../models');
var crypto = require('crypto');

// Get all users
router.get('/', function(req, res) {
  models.User.findAll({ include: [{ all: true, nested: true }] })
  .then(function(users) {
    if (users)
      res.json(users);
    else
      res.status(401).send('Users not found');
  });
});

// Get one user information by userid
router.get('/:userid',function(req,res){
  models.User.findOne(
    {
      where: {id: req.params.userid},
      include: [{ all: true, nested: true }]
    }
  ).then(function(user) {
    if (user)
      res.json(user);
    else
      res.status(401).send('User not found');
  });
});

// Get one user by username and password
router.post('/login',function(req,res){
  models.User.findOne({where: { username: req.body.username, password: crypto.createHash('md5').update(req.body.password).digest("hex") }}).then(function(user) {
    if (user)
      res.json(user);
    else
      res.status(401).send('The username or the password is incorrect');
  });
});

// Delete one user by userid
router.delete('/:userid',function(req,res){
  models.User.findOne({where: {id: req.params.userid}}).then(function(user) {
    if (user) {
      user.destroy();
      res.status(200).send('User deleted');
    }
    else
      res.status(401).send('User not found');
  });
});

// Update one user by userid
router.put('/:userid',function(req,res){
  models.User.findOne({where: {id: req.params.userid}}).then(function(user) {
    if (user) {
      user.update({
        username: req.body.username,
        password: crypto.createHash('md5').update(req.body.password).digest("hex"),
        email: req.body.email
      }).then(function(response) {
        if(response)
          res.status(200).send(response);
        else
          res.status(401).send('ERROR: User not updated');
      });
    }
  });
});

// Insert one user
router.post('/',function(req,res){
  return models.User.create({
    username: req.body.username,
    password: crypto.createHash('md5').update(req.body.password).digest("hex"),
    email: req.body.email
  }).then(function(response){
    if(response)
      res.status(200).send(response);
    else
      res.status(401).send('ERROR: User not inserted');
  });
});

module.exports = router;
