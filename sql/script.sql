-- USERS
INSERT INTO "public"."Users"("id", "username", "password", "email", "createdAt", "updatedAt") VALUES('1', 'user1', 'pass1', 'user1@gmail.com', now(), now()) RETURNING "id", "username", "password", "email", "createdAt", "updatedAt";
INSERT INTO "public"."Users"("id", "username", "password", "email", "createdAt", "updatedAt") VALUES('2', 'user2', 'pass2', 'user2@gmail.com', now(), now()) RETURNING "id", "username", "password", "email", "createdAt", "updatedAt";

-- ROOMS
INSERT INTO "public"."Rooms"("id", "name", "description", "createdAt", "updatedAt", "UserId") VALUES('1', 'Living Room', 'Living Room description', now(), now(), '1') RETURNING "id", "name", "description", "createdAt", "updatedAt", "UserId";
INSERT INTO "public"."Rooms"("id", "name", "description", "createdAt", "updatedAt", "UserId") VALUES('2', 'Bedroom 1', 'Bedroom 1 description', now(), now(), '1') RETURNING "id", "name", "description", "createdAt", "updatedAt", "UserId";
INSERT INTO "public"."Rooms"("id", "name", "description", "createdAt", "updatedAt", "UserId") VALUES('3', 'Kitchen', 'Kitchen description', now(), now(), '2') RETURNING "id", "name", "description", "createdAt", "updatedAt", "UserId";

-- TYPES
INSERT INTO "public"."Types"("id", "type", "createdAt", "updatedAt") VALUES('1', 'Humidity', now(), now()) RETURNING "id", "type", "createdAt", "updatedAt";
INSERT INTO "public"."Types"("id", "type", "createdAt", "updatedAt") VALUES('2', 'Temperature', now(), now()) RETURNING "id", "type", "createdAt", "updatedAt";
INSERT INTO "public"."Types"("id", "type", "createdAt", "updatedAt") VALUES('3', 'Light', now(), now()) RETURNING "id", "type", "createdAt", "updatedAt";
INSERT INTO "public"."Types"("id", "type", "createdAt", "updatedAt") VALUES('4', 'Heating', now(), now()) RETURNING "id", "type", "createdAt", "updatedAt";

-- SENSORS
INSERT INTO "public"."Sensors"("id", "name", "description", "createdAt", "updatedAt", "RoomId", "TypeId") VALUES('1', 'Humidity', 'Humidity description', now(), now(), '1', '1') RETURNING "id", "name", "description", "createdAt", "updatedAt", "RoomId", "TypeId";
INSERT INTO "public"."Sensors"("id", "name", "description", "createdAt", "updatedAt", "RoomId", "TypeId") VALUES('2', 'Temperature', 'Temperature description', now(), now(), '1', '2') RETURNING "id", "name", "description", "createdAt", "updatedAt", "RoomId", "TypeId";
INSERT INTO "public"."Sensors"("id", "name", "description", "createdAt", "updatedAt", "RoomId", "TypeId") VALUES('3', 'Temperature', 'Temperature description', now(), now(), '2', '2') RETURNING "id", "name", "description", "createdAt", "updatedAt", "RoomId", "TypeId";
INSERT INTO "public"."Sensors"("id", "name", "description", "createdAt", "updatedAt", "RoomId", "TypeId") VALUES('4', 'Light 1', 'Light 1 description', now(), now(), '1', '3') RETURNING "id", "name", "description", "createdAt", "updatedAt", "RoomId", "TypeId";

-- VALUES
INSERT INTO "public"."Values"("id", "value", "createdAt", "updatedAt", "SensorId") VALUES('1', '35', now(), now(), '1') RETURNING "id", "value", "createdAt", "updatedAt", "SensorId";
INSERT INTO "public"."Values"("id", "value", "createdAt", "updatedAt", "SensorId") VALUES('2', '28', now(), now(), '2') RETURNING "id", "value", "createdAt", "updatedAt", "SensorId";
INSERT INTO "public"."Values"("id", "value", "createdAt", "updatedAt", "SensorId") VALUES('3', '47', now(), now(), '1') RETURNING "id", "value", "createdAt", "updatedAt", "SensorId";
INSERT INTO "public"."Values"("id", "value", "createdAt", "updatedAt", "SensorId") VALUES('4', '1', now(), now(), '4') RETURNING "id", "value", "createdAt", "updatedAt", "SensorId";
INSERT INTO "public"."Values"("id", "value", "createdAt", "updatedAt", "SensorId") VALUES('5', '25', now(), now(), '3') RETURNING "id", "value", "createdAt", "updatedAt", "SensorId";
INSERT INTO "public"."Values"("id", "value", "createdAt", "updatedAt", "SensorId") VALUES('6', '85', now(), now(), '5') RETURNING "id", "value", "createdAt", "updatedAt", "SensorId";