var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');

var routes = require('./routes');
var all = require('./routes/all');
var users = require('./routes/users');
var sensors = require('./routes/sensors');
var types = require('./routes/types');
var values = require('./routes/values');
var rooms = require('./routes/rooms');

var app = express();

app.set('json spaces', 2);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", 'Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/sensors', sensors);
app.use('/types', types);
app.use('/values', values);
app.use('/rooms', rooms);
app.use('/all', all);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
// no stacktraces leaked to user unless in development environment
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: (app.get('env') === 'development') ? err : {}
    });
});

module.exports = app;